let bcrypt = require('bcrypt')

const jwt = require('jsonwebtoken')

const listRouter = require('../myframe/router')()
const users = require('../models/users')



listRouter.post('/register', (req, res) => {

  let {email, password} = req.body
  let password_hash = bcrypt.hashSync(password, 15)

  let newUser = {
    email,
    password_hash
  }

  users.create(newUser)
    .then(user => {
      user.password_hash = undefined
      res.json(user)
    })
    .catch(err => res.json(err))

})

listRouter.post('/login', (req, res) => {
  let {email, password} = req.body

  users.findOne({where: {email}})
    .then(user => {
      if (!bcrypt.compareSync(password, user.password_hash)) {
        res.json({message: 'Authentication failed. Wrong password.'})
      }
      const token = jwt.sign({userId: user.id }, 'secret', { expiresIn: '1m' })
      res.setHeader('Authorization', 'JWT ' + token)
      res.json('ok')
    })
    .catch(err => res.json(err))
})

listRouter.post('/logout', (req, res) => {

})

listRouter.post('/test', (req, res) => {


  // let token = req.headers.authorization.split(' ')[1]
  //
  // jwt.verify(token, 'secret', (err, verifiedJwt) => {
  //
  // //  verifiedJwt.exp += 60*60
  //   res.json(err||verifiedJwt)
  // })



 cards.getOwnerId(2)
   .then( data => res.json(data.list.bord.user.id))
})

module.exports = listRouter